/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.shapeinheritance;

/**
 *
 * @author WIP
 */
public class Circle extends Shape {
    private double r;
    private double pi = 22.0/7;
    
    public Circle(double r){
        this.r = r;
    }
    
    public double calArea(){
        return pi * r * r;
    }
    
    public void print(){
        System.out.println("This Circle: r: " + this.r + ", area = " + this.calArea());
    }
    
    public double getR(){
        return this.r;
    }
    
    public void setR(double r){
        if(r<=0) {
            System.out.println("Error: Raduis must more than zero!!");
            return;
        }
        this.r = r;
    }
    
}
