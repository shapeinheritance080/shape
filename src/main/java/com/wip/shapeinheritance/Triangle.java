/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.shapeinheritance;

/**
 *
 * @author WIP
 */
public class Triangle extends Shape{
    private double base;
    private double height;
    
    public Triangle(double base, double height){
        this.base = base;
        this.height = height;
    }
    
    public double calArea(){
         return 0.5 * (base*height);
    }
    
    public void print(){
        System.out.println("This Triangle: base: " +  this.base +
                  ", height: " + this.height + ", area = " + this.calArea());
    }
    
    public void setBase(double base){
        if(base<=0){
            System.out.println("Error: Base must more than zero");
            return;
        }
        this.base = base;
    }
    public void setHeight(double height){
        if(height<=0){
            System.out.println("Error: Heigh must more than zero");
            return;
        }
        this.height = height;
    }
    public double getBase(){
        return this.base = base;
    }
    public double getHeight(){
        return this.height = height;
    }
}
