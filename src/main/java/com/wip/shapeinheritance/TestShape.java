/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.shapeinheritance;

/**
 *
 * @author WIP
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape = new Shape();
        System.out.println("Shape: " + shape);
        //1
        Circle circle1 = new Circle(3);
        System.out.println("circle1 area is " + circle1.calArea());
        circle1.print();
        //2
        Circle circle2 = new Circle(4);
        System.out.println("circle2 area is " + circle2.calArea());
        
        System.out.println("circle1 is Shape: " + (circle1 instanceof Shape));
        System.out.println("circle2 is Shape: " + (circle2 instanceof Shape));
        System.out.println("shape is Circle: " + (shape instanceof Circle));
        System.out.println("shape is Shape: " + (shape instanceof Shape));
        
        System.out.println("");
        //3
        Triangle triangle1 = new Triangle(4, 3);
        System.out.println("triangle1 area is " + triangle1.calArea());
        
        System.out.println("triangle1 is Shape: " + (triangle1 instanceof Shape));
        System.out.println("shape is Triangle: " + (shape instanceof Triangle));
        
        System.out.println("");
        //4
        Rectangle rectangle1 = new Rectangle(3, 4);
        System.out.println("rectangle1 area is " + rectangle1.calArea());
        
        System.out.println("rectangle1 is Shape: " + (rectangle1 instanceof Shape));
        System.out.println("shape is Rectangle: " + (shape instanceof Rectangle));
        
        System.out.println("");
        //5
        Square square1 = new Square(2);
        System.out.println("square1 area is " + square1.calArea());
        
        System.out.println("square1 is Shape: " + (square1 instanceof Shape));
        System.out.println("shape is Square: " + (shape instanceof Square));
        System.out.println("square1 is Rectangle: " + (square1 instanceof Rectangle));
        
        System.out.println("");
        //Loop
        Shape[] shapeIn = {circle1, circle2, triangle1, rectangle1, square1};
        
        for(int i=0; i<shapeIn.length; i++){
            shapeIn[i].print();
            
        }

    }
}
