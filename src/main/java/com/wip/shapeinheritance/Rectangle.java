/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.shapeinheritance;

/**
 *
 * @author WIP
 */
public class Rectangle extends Shape{
    protected double width;
    protected double height;
    public Rectangle(double width, double height){
        this.width = width;
        this.height = height;
    }
    
    public double calArea(){
        return width * height;
    }
    
    public void print(){
        System.out.println("This Rectangle: width: " + this.width + ", height: " 
                           + this.height + ", area = " + this.calArea());
    }
    
    public void setWidth(double width){
        if(width<=0){
            System.out.println("Error: wide must more than 0");
            return;
        }
        this.width = width;
    }
    public void setHeight(double height){
        if(height<=0){
            System.out.println("Error: long must more than 0");
            return;
        }
        this.height = height;
    }
    public double getWidth(){
        return this.width;
    }
    public double getHeight(){
        return this.height;
    }
}
