/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.shapeinheritance;

/**
 *
 * @author WIP
 */
public class Square extends Rectangle{
    private double side;
    public Square(double side){
        super(side, side);
        this.side = side;
    }
    
    public double calArea(){
        return side*side;
    }
    
    public void print(){
        System.out.println("This Square: side: " + this.side + ", area = " 
                           + this.calArea());
    }
    
    public void setSide(double side){
        if(side<=0){
            System.out.println("Error: square must more than zero");
            return;
        }
        this.side = side;
    }
    public double getS(){
        return this.side = side;
    }
}
